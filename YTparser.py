# Programa que obtiene información de un xml de Youtube.
# Importamos las librerias necesarias
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
from urllib.request import urlopen

# Variable global que va a contener los videos
videos = ''

# HTML que vamos a generar
html = """
<!DOCTYPE html>
<html lang="en">
    <body>
    <h1>Channel contents:</h1>
    <ul>
        {videos}
    </ul>
  </body>
</html>
"""

# Clase para manejar los contenidos del xml
class Handler(ContentHandler):

    def __init__(self):
        self.inEntry = False
        self.inContent = False
        self.link = ""
        self.title = ""
        self.content = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True

        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')

    def endElement(self, name):
        global videos

        if name == 'entry':
            self.inEntry = False
            videos = videos + '<li><a href="' + self.link + '">' + self.title + '</a></li>\n'
        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars


def xmlYT(id):
    urlXml = "https://www.youtube.com/feeds/videos.xml?channel_id=" + id
    # -- Abrimos el fichero xml
    xmlYTChannel = urlopen(urlXml)
    return xmlYTChannel

Parser = make_parser()
YT_Handler = Handler()
Parser.setContentHandler(YT_Handler)

# -- Main prog
if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Usage: python3 YTparser.py <id>")
        print(" ")
        sys.exit(1)
    # Obtenemos el xml
    xmlcode = xmlYT(sys.argv[1])

    Parser.parse(xmlcode)
    pagina = html.format(videos=videos)
    print(pagina)
